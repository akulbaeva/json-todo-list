import {EDIT_TITLE, GET_POSTS} from "../actions/action.type";

const initialState = {
    posts: []
}

const postReducer = (state = initialState, action) => {
    if (action.type === GET_POSTS) {
        state = {
            ...state,
            posts: action.posts.map(it => {
                    return {
                        id: it.id,
                        title: it.title,
                        userName: it.user.map(user => {
                            if (user.id === it.userId) {
                                return user.name
                            }
                        })
                    }
                }
            )
        }
    } else if (action.type === EDIT_TITLE) {
        return  {
            ...state,
            newTitle: state.posts.map(it => {
                if (action.data.id === it.id) {
                    it.title = action.data.title
                    return action.data.title
                }
            })
        }
    }
    return state
}

export default postReducer