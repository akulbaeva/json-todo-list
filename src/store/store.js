import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk'
import postReducer from "../reducers/postReducer";

const store = createStore(
    postReducer,
    applyMiddleware(thunkMiddleware)
);

export default store