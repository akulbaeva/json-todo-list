import {EDIT_TITLE, GET_POSTS, postURL, userURL} from "./action.type";

export const getPosts = () => dispatch => {
    fetch(postURL)
        .then(response => response.json())
        .then(data => {
            Promise.all(data.map(it => {
                    return fetch(userURL)
                        .then(response => response.json())
                        .then(user => ({...it, user: user}))
                }
            )).then(posts => {
                dispatch({
                    type: GET_POSTS,
                    posts
                })
            })
        })
        .catch(e => {
            console.log('Error' + e)
        })
}

export const editTitle = (id, data) => dispatch => {
    fetch(postURL + '/' + id, updatedPost(data))
        .then(response => response.json())
        .then(data => {
            dispatch({
                    type: EDIT_TITLE,
                    data
                }
            )
        })
}

const updatedPost = (data) => ({
        method: 'PUT',
        headers: {'Content-type': 'application/json; charset=UTF-8'},
        body: JSON.stringify({
            title: data
        })
    }
)