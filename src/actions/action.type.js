export const postURL = 'https://jsonplaceholder.typicode.com/posts'
export const userURL = 'https://jsonplaceholder.typicode.com/users'
export const GET_POSTS = "GET_POSTS"
export const EDIT_TITLE = "EDIT_TITLE"