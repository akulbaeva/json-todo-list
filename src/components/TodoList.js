import React, {useEffect} from "react";
import Post from "./Post";
import {editTitle, getPosts} from "../actions/actions";
import {connect} from "react-redux";


const TodoList = (props) => {
    useEffect(() => {
        props.getPosts()
    })

    return (
        <div>
            {props.posts.map(post => (
                    <Post
                        title={post.title}
                        userName={post.userName}
                        key={post.id}
                        editTitle={props.editTitle}
                    />
                )
            )}
        </div>
    )
}

const mapStateToProps = (state) => ({
    posts: state.posts,
    newTitle: state.newTitle
})


const mapDispatchToProps = (dispatch) => ({
    getPosts: () => dispatch(getPosts()),
    editTitle: (id, newTitle) => dispatch(editTitle(id, newTitle))
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)