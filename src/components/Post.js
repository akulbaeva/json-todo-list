import React, {useState} from "react";

const Post = props => {
    const [editState, setEditState] = useState(false)
    const [value, setValue] = useState("")

    const edit = (e) => {
        e.preventDefault()
        if (!value) return
        props.editTitle(1, value)
        setValue('')
        setEditState(false)
    }

    return (
        <div>
            {editState ?
                (<div><input type="text"
                             onChange={(e) => setValue(e.target.value)}
                             value={value}/>
                    <button onClick={edit}>EDIT</button>
                </div>) :
                (<h5 onClick={() => setEditState(true)}>TITLE: {props.title}</h5>)
            }
            <h5>USERNAME: {props.userName}</h5>
            <hr/>
        </div>
    );
};

export default Post

